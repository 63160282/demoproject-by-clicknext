interface Question {
    question: string;
    choices: string[];
    correctAnswer: number;
  }
  
  const questions: Question[] = [
    {
      question: 'What is the output of the following code?\n\nconsole.log(typeof null);',
      choices: ['"object"', '"null"', '"undefined"', '"boolean"'],
      correctAnswer: 0,
    },
    {
      question: 'Which method is used to add one or more elements to the end of an array?',
      choices: ['push()', 'join()', 'slice()', 'concat()'],
      correctAnswer: 0,
    },
    {
        question: 'What is the result of the following expression?\n\n3 + 2 + "7"',
        choices: ['"327"', '"12"', '"57"', '"NaN"'],
        correctAnswer: 2,
      },
      {
        question: 'What is the purpose of the "use strict" directive in JavaScript?',
        choices: ['Enforce stricter type checking', 'Enable the use of modern syntax', 'Enable strict mode for improved error handling', 'Disable certain features for better performance'],
        correctAnswer: 2,
      },
      {
        question: 'What is the scope of a variable declared with the "let" keyword?',
        choices: ['Function scope', 'Global scope', 'Block scope', 'Module scope'],
        correctAnswer: 2,
      },
      {
        question: 'Which higher-order function is used to transform elements of an array into a single value?',
        choices: ['map()', 'filter()', 'reduce()', 'forEach()'],
        correctAnswer: 2,
      },
      {
        question: 'What does the "=== " operator in JavaScript check for?',
        choices: ['Equality of values', 'Equality of values and types', 'Inequality of values', 'Reference equality'],
        correctAnswer: 1,
      },
      {
        question: 'What is the purpose of the "this" keyword in JavaScript?',
        choices: ['Refer to the current function', 'Refer to the parent function', 'Refer to the global object', 'Refer to the object that owns the current code'],
        correctAnswer: 3,
      },
      {
        question: 'What does the "NaN" value represent in JavaScript?',
        choices: ['Not a Number', 'Null', 'Negative Number', 'Not Applicable'],
        correctAnswer: 0,
      },
      {
        question: 'Which method is used to remove the last element from an array?',
        choices: ['pop()', 'shift()', 'slice()', 'splice()'],
        correctAnswer: 0,
      },
  ];
  
  const divv = document.getElementById('ex2') as HTMLDivElement;
  
  const createQuiz = () => {
    const quizContainer = document.createElement('div');
    quizContainer.classList.add('quiz-container');
    divv.appendChild(quizContainer);
  
    const scoreText = document.createElement('p');
    let score = 0;
    scoreText.textContent = Current Score: ${score}/10;
    quizContainer.appendChild(scoreText);
  
    questions.forEach((question, questionIndex) => {
      const questionText = document.createElement('p');
      questionText.textContent = question.question;
      quizContainer.appendChild(questionText);
  
      const choicesContainer = document.createElement('div');
      choicesContainer.classList.add('choices-container');
      quizContainer.appendChild(choicesContainer);
  
      const radioButtons: HTMLInputElement[] = [];
  
      question.choices.forEach((choice, choiceIndex) => {
        const choiceContainer = document.createElement('div');
        choiceContainer.classList.add('choice-container');
        choicesContainer.appendChild(choiceContainer);
  
        const radio = document.createElement('input');
        radio.type = 'radio';
        radio.name = question-${questionIndex};
        radio.value = choiceIndex.toString();
        choiceContainer.appendChild(radio);
  
        const label = document.createElement('label');
        label.textContent = choice;
        choiceContainer.appendChild(label);
  
        radioButtons.push(radio);
      });
  
      const submit = document.createElement('button');
      submit.textContent = 'Submit';
      choicesContainer.appendChild(submit);
  
      submit.addEventListener('click', () => {
        const selectedRadio = radioButtons.find((radio) => radio.checked);
  
        if (selectedRadio) {
          const selectedChoiceIndex = parseInt(selectedRadio.value);
          radioButtons.forEach((radio) => (radio.disabled = true));
  
          if (selectedChoiceIndex === question.correctAnswer) {
            score++;
          }
  
          scoreText.textContent = Current Score: ${score}/10;
          submit.disabled = true;
          radioButtons.forEach((radio) => (radio.disabled = true));
        }
      });
    });
  };
  
  createQuiz();
  