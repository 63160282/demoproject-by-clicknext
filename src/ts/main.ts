
interface Address{
    street: string
    city: string
    country: string
}
interface Person{
    name: string
    age: number
    address:Address
    hobbies:string[]
}
const n:number = 10
const s:string = 'text'
const c:boolean = true


 const person: Person = {
    name: 'John',
    age: 25,
    address:{
        street:'123 Main Street',
        city: 'bangsean',
        country: 'thai'
    },
    hobbies:['coding,phone']
 }
let address:Address={
    street:'123 Main Street',
    city: 'bangsean',
    country: 'thai'
 }
 person.address=address

function add(number1:number,number2:number):number{
    return number1+number2
}
function DisplayDetail(person:Person):string{
    return 'name='+person.name+'; age='+person.age+
    "; address="
    +person.address.street
    +person.address.city
    +person.address.country
    +'; hobbies='+person.hobbies.join(',')
}
console.log(DisplayDetail({
    name:'john',
    age: 21 ,
    address:{
        city:'เมือง',
        country:'ไทย',
        street:'หาดบางแสน',
    },
    hobbies:['ดูการ์ตูน','นอน']
}))
const addresses: Address[]=[
    {
        city:'เมืองชลบุรี1',
        country:'ประเทศชลบุรี',
        street:'หาดบางแสน',
    },
    {
        city:'เมืองชลบุรี2',
        country:'ประเทศชลบุรี',
        street:'หาดบางแสน',
    }
    ,
    {
        city:'เมืองชลบุรี3',
        country:'ประเทศชลบุรี',
        street:'หาดบางแสน',
    },
    {
        city:'เมืองชลบุรี4',
        country:'ประเทศชลบุรี',
        street:'หาดบางแสน',
    }
]

 export { }